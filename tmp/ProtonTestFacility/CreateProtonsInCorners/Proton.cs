﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.IO;
using System.Windows.Input;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Timers;
using System.Windows.Threading;

namespace ProtonCollider.Model
{
    public enum Player { one, two };
    public enum ProtonState {
        initialised,
        loading,
        accelerating,
        moving,
        colliding,
        destroyed
    };

    public class Proton : ScatterViewItem
    {
        private const string _PROTON_VIDEO_SRC = @"Resources/proton_animation.mp4";
        public const int PROTON_RADIUS = 75;

        private MediaElement protonVideo;
        private ProtonState state;
        private Player player;
        private ProtonTracker protonTracker;
        private Point initialCenterForResettingProton;

        // Members for rotating animation of the proton
        private const double MAX_ROTATION_SPEED_RATIO = 4.0;
        private const double ROTATION_SPEED_UP_FACTOR = 1.08;  /// Responsible for accelerating the rotation
        private double currentRotationSpeedRatio = 1;
        private Storyboard rotationStoryboard;
        Timer timerToSpeedUpRotation = new Timer();

        // Members for moving animation of the proton
        private Storyboard movingStoryboard;
        private Point directionVector;
        private double acceleration;
       

        public Player User
        {
            get
            {
                return player;
            }
            private set
            {
                player = value;
            }
        }

        private ProtonState State
        {
            get
            {
                return state;
            }
            set
            {
                ProtonState previousState = state;
                state = value; 
                onStateChanged(previousState);
            }
        }


        public Proton(Player player)
        {
            this.State = ProtonState.initialised;
            this.protonTracker = new ProtonTracker();
            this.User = player;
            this.CanRotate = true;
            this.CanScale = false;
            this.CanMove = false;

            this.Height = this.Width = PROTON_RADIUS * 2;

            // Clip viewable content to a circle
            double center = this.Height / 2;
            double radius = this.Height / 3.7; // 3.7 is a tested value
            EllipseGeometry circle = new EllipseGeometry(new Point(center, center), radius, radius);
            this.Clip = circle;

            // Set the Content to the video.
            setProtonVideo();
        }

        private void setProtonVideo()
        {
            // Create a MediaElement object.
            protonVideo = new MediaElement();
            protonVideo.LoadedBehavior = MediaState.Manual;
            protonVideo.UnloadedBehavior = MediaState.Manual;

            protonVideo.Source = new Uri(_PROTON_VIDEO_SRC, UriKind.Relative);
            protonVideo.Play();

            protonVideo.MediaEnded += new RoutedEventHandler(this.replayVideo);
            this.Content = protonVideo;

        }

        private void replayVideo(object sender, EventArgs e)
        {
            protonVideo.Position = TimeSpan.FromSeconds(0);
            protonVideo.Play();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e) 
        {
            startLoading();
            // TODO: otherwise the the proton would be moved --> would automatically go to state accelerating
            base.OnMouseDown(e);
        }
        protected override void OnTouchDown(TouchEventArgs e)
        {
            startLoading();
            base.OnTouchDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                e.Handled = true;
                return;
            }
            onMove(e.GetPosition(Application.Current.MainWindow));
            base.OnMouseMove(e);
        }
        protected override void OnTouchMove(TouchEventArgs e)
        {
            onMove(e.GetTouchPoint(Application.Current.MainWindow).Position);
            base.OnTouchMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            onTouchUp(e.GetPosition(Application.Current.MainWindow));
            base.OnMouseUp(e);
        }
        protected override void OnTouchUp(TouchEventArgs e)
        {
            onTouchUp(e.GetTouchPoint(Application.Current.MainWindow).Position);
            base.OnTouchUp(e);
        }

        private void startLoading()
        {            
           this.State = ProtonState.loading;
        }

        private void onTouchUp(Point touchUpPoint)
        {
            directionVector = protonTracker.getDirectionVector();
            acceleration = protonTracker.getAcceleration(directionVector);

            // TODO: remove
            Console.WriteLine("DirVec: " + directionVector);
            Console.WriteLine("Accel: " + acceleration);


            if (1000 >acceleration) // 1000 is a tested value
            {
                this.State = ProtonState.initialised;
            }
            if (ProtonState.accelerating == this.State)
            {
                this.State = ProtonState.moving;
            }
        }

        private void onMove(Point movedPosition)
        {
            Debug.Assert(ProtonState.loading == this.State || ProtonState.accelerating == this.State);

            if (ProtonState.loading == this.State)
            {
                this.State = ProtonState.accelerating;
            }

            this.Center = movedPosition;

            protonTracker.addMovePoint(this.Center);

            if (this.reachedAccelerationBorder())
                this.State = ProtonState.moving;
        }

        /// <summary>
        /// Determines if the proton reached the Acceleration Border.
        /// The acceleration border is the area in which the user can control the proton and depends
        /// on the player.
        /// If the user moves a proton to this border then the proton will go into the moving state.
        /// </summary>
        /// <returns>true if the proton reaches the border of the acceleration area</returns>
        private bool reachedAccelerationBorder()
        {
            return false;
        }

        /// <summary>
        /// Action after a state change. Can be used to initiate initiating and closing (of the old state) operations of the respective state.
        /// </summary>
        private void onStateChanged(ProtonState previousState)
        {
            //TODO: remove
            Console.WriteLine("State:" + this.State + "\n");

            // closing actions
            switch (this.State)
            {
                case ProtonState.initialised:
                    break;
                case ProtonState.loading:

                    break;
                case ProtonState.accelerating:
                    break;
                case ProtonState.moving:
                    break;
                case ProtonState.colliding:
                    break;
                case ProtonState.destroyed:
                    break;
            }

            // initiating actions
            switch (this.State)
            {
                case ProtonState.initialised:
                    stopLoadingIfNecessary();
                    break;
                case ProtonState.loading:
                    initiateLoading();
                    break;
                case ProtonState.accelerating:
                    initiateAcceleration();
                    break;
                case ProtonState.moving:
                    initiateMoving();
                    break;
                case ProtonState.colliding:
                    initiateCollsision();
                    break;
                case ProtonState.destroyed:
                    resetToStart();
                    break;
            }
        }

        private void stopLoadingIfNecessary()
        {
            if (null != rotationStoryboard)
                rotationStoryboard.Pause(this);
        }

        private void initiateLoading() 
        {
            Debug.Assert(ProtonState.loading == this.State);

            // If we have no storyboard yet then we create one (per Proton) else we resume the paused storyboard
            if (null == rotationStoryboard)
            {
                // Animation
                Duration duration = new Duration(TimeSpan.FromSeconds(1));
                DoubleAnimation rotationAnimation = new DoubleAnimation(0.0, 360.0, duration);
                rotationAnimation.RepeatBehavior = RepeatBehavior.Forever;
                rotationAnimation.AutoReverse = false;

                // Storyboard
                rotationStoryboard = new Storyboard();
                rotationStoryboard.Children.Add(rotationAnimation);
                Storyboard.SetTarget(rotationAnimation, this);
                Storyboard.SetTargetProperty(rotationAnimation, new PropertyPath(ScatterViewItem.OrientationProperty));
                rotationStoryboard.Begin(this, true);

                // Timer to increase speed of rotation to indicate the loading process
                timerToSpeedUpRotation = new Timer();
                timerToSpeedUpRotation.Elapsed += new ElapsedEventHandler(OnTimedSpeedUpRotationEvent);
                timerToSpeedUpRotation.Interval = 100;  // [ms]
                timerToSpeedUpRotation.Enabled = true;
            }
            else
            {
                // Reset rotation speed and enable timer again
                currentRotationSpeedRatio = 1;
                rotationStoryboard.SetSpeedRatio(this, currentRotationSpeedRatio);
                timerToSpeedUpRotation.Enabled = true;

                rotationStoryboard.Resume(this);
            } 
        }

        private void OnTimedSpeedUpRotationEvent(object source, ElapsedEventArgs e)
        {
            if (currentRotationSpeedRatio < MAX_ROTATION_SPEED_RATIO)
            {
                currentRotationSpeedRatio *= ROTATION_SPEED_UP_FACTOR;
                rotationStoryboard.SetSpeedRatio(this, currentRotationSpeedRatio);
            }
        }

        private void leavingLoadState()
        {
            // Stop timer
            timerToSpeedUpRotation.Stop();
        }

        private void initiateAcceleration()
        {
            Debug.Assert(ProtonState.accelerating == this.State);
        }

        private void initiateMoving()
        {
            Debug.Assert(ProtonState.moving == this.State);

            startMoveAnimation();
        }

        private void startMoveAnimation()
        {
            // Start moving animation
            PointAnimation movingAnimation = new PointAnimation();
            movingAnimation.From = this.ActualCenter;

            const double MAX_ACCEPTED_ACCELERATION = 50000;
            double accelerationEnergy = 5 * (Math.Min(acceleration, MAX_ACCEPTED_ACCELERATION) / MAX_ACCEPTED_ACCELERATION);
            double rotationEnergy = currentRotationSpeedRatio;
            double deltaX = directionVector.X * accelerationEnergy * rotationEnergy;
            double deltaY = directionVector.Y * accelerationEnergy * rotationEnergy;

            double squarredNorm = deltaX * deltaX + deltaY * deltaY;
            const double MIN_SQUARED_NORM = 2000 * 2000; // 2000 Corresponds to min way to move in pixels

            // Check to assure the proton moves outside the screen
            // Idea: if the way is too short then the way with a correction factor.
            if (MIN_SQUARED_NORM > squarredNorm)
            {
                // The proton will be to slow. We need to 'stretch' the vector
                deltaX *= MIN_SQUARED_NORM / squarredNorm;
                deltaY *= MIN_SQUARED_NORM / squarredNorm;
            }

            double toX = this.ActualCenter.X + deltaX;
            double toY = this.ActualCenter.Y + deltaY;

            movingAnimation.To = new Point(toX, toY);
            Storyboard.SetTarget(movingAnimation, this);
            Storyboard.SetTargetProperty(movingAnimation, new PropertyPath(ScatterViewItem.CenterProperty));

            movingStoryboard = new Storyboard();
            movingStoryboard.Children.Add(movingAnimation);
            movingStoryboard.FillBehavior = FillBehavior.Stop;
            movingStoryboard.Completed += (s, args) =>
            {
                this.Center = this.ActualCenter;
                this.State = ProtonState.destroyed;
            };
            movingStoryboard.Begin();
        }

        private void initiateCollsision()
        {
            Debug.Assert(ProtonState.colliding == this.State);

            this.State = ProtonState.destroyed;
            // TODO: animate a collision
        }

        private void resetToStart() 
        {
            if(movingStoryboard != null)
                movingStoryboard.Stop();
            this.Center = this.initialCenterForResettingProton;
            currentRotationSpeedRatio = 1;
            this.State = ProtonState.initialised;
            timerToSpeedUpRotation.Enabled = false;

        }

        internal void setInitialCenter(Point point)
        {
            this.Center = point;
            this.initialCenterForResettingProton = point;
        }

        public bool isMoving()
        {
            return this.State == ProtonState.moving;
        }

        internal void destroy()
        {
            this.State = ProtonState.destroyed;
        }

        internal double getVelocityEnergy()
        {
            return acceleration;   
        }

        internal double getRotationEnergy()
        {
            return currentRotationSpeedRatio;
        }
    }

    /// <summary>
    /// Responsible for setting the position of protons, depending on the number of the user 
    /// </summary>
    public class ProtonPositioner
    {
        private SurfaceWindow window;
        /// <summary>
        /// The parameter describes the postion of the Proton on the screen
        /// along the diagonal.
        /// If we have a diagonal of 100px and a value of 10 percent, then
        /// the Proton will be set to 10 px from the lower corner.
        /// </summary>
        private int percentageOfDiagonal = 20;

        public ProtonPositioner(SurfaceWindow window)
        {
            this.window = window;
        }

        public void setPositionOf(Proton proton)
        {
            // double diagonal = Math.Sqrt(window.Height * window.Height + window.Width * window.Width);
            double diagonal = Math.Sqrt(window.Height * window.Height * 2);
            // double angleOfDiagonal = Math.Atan(window.Height / window.Width); // atan(opposite/adjacent)
            double angleOfDiagonal = Math.Atan(window.Height / window.Height); // atan(opposite/adjacent)
            double diagonalPositon = diagonal * ((Double) percentageOfDiagonal / 100);
            double distanceFromX = diagonalPositon * Math.Cos(angleOfDiagonal);
            double distanceFromY = diagonalPositon * Math.Sin(angleOfDiagonal);

            switch (proton.User)
            {
                case Player.one:
                    // lower left corner
                    proton.setInitialCenter(new Point(distanceFromX, window.Height - distanceFromY));
                    break;
                case Player.two:
                    // lower right corner
                    proton.setInitialCenter(new Point(window.Width - distanceFromX, window.Height - distanceFromY));
                    break;
                default:
                    throw new NotImplementedException("Added a new user?");
            }        
        }

    }

    /// <summary>
    /// The ProtonTracker tracks the position of the Proton by saving a specific amount(NR_POINTS_TO_TRACK) of points.
    /// These Points are used to calculate the direction and acceleration for the moving animation.
    /// </summary>
    class ProtonTracker
    {
        public const int NR_POINTS_TO_TRACK = 100;

        private int counterForTrackedPoints = 0;  /// Is used in the case when trackedPoints smaller NR_POINTS_TO_TRACK. Then we can only use trackedPoints for the computation.
        private int indexToNextInsertPosition = 0;
        private Point[] trackedPoints;

        public ProtonTracker()
        {
            trackedPoints = new Point[NR_POINTS_TO_TRACK];
        }

        /// <summary>
        /// Resets the tracker
        /// </summary>
        public void reset()
        {
            counterForTrackedPoints = 0;
            indexToNextInsertPosition = 0;
        }

        public void addMovePoint(Point pointToAdd)
        {
            trackedPoints[indexToNextInsertPosition] = pointToAdd;
            indexToNextInsertPosition = (indexToNextInsertPosition + 1) % NR_POINTS_TO_TRACK;
            ++counterForTrackedPoints;
        }

        /// <summary>
        /// Calculates the acceleration of the directionPoint.
        /// The acceleration corresponds to the squared norm (length) of the directionVector.
        /// We take the squared acceleration to avoid squareroot computation.
        /// We don't compute the directionVector from tracked points due to performance issues.
        /// (The direction vector has been calculated already, so we don't need to calculate it again) 
        /// </summary>
        /// <returns>Returns an acceleration factor > 1.0, (the squared norm of the given direction vector)</returns>
        public double getAcceleration(Point directionVector)
        {
            double squaredNorm = directionVector.X * directionVector.X + directionVector.Y * directionVector.Y;
            return Math.Max(1.0, squaredNorm);
        }

        /// <summary>
        /// Calculates the direction of the last tracked points.
        /// (max. NR_POINTS_TO_TRACK).
        /// </summary>
        /// <returns>a Point </returns>
        public Point getDirectionVector()
        {
            // We are calculating the direction vector by adding the corresponding delta (offset to the previous point)
            // to the new point in each direction (X,Y) separately.
            int minPoints = Math.Min(NR_POINTS_TO_TRACK, counterForTrackedPoints);
            double deltaX = 0.0;
            double deltaY = 0.0;
            int indexCurrentPoint;
            int indexPreviousPoint;
            if (NR_POINTS_TO_TRACK > counterForTrackedPoints)
            {  // We have tracked less than NR_POINTS_TO_TRACK 
                indexPreviousPoint = 0;

                for (int i = 1; i < counterForTrackedPoints; ++i)
                {
                    indexCurrentPoint = i;

                    deltaX += trackedPoints[indexCurrentPoint].X - trackedPoints[indexPreviousPoint].X;
                    deltaY += trackedPoints[indexCurrentPoint].Y - trackedPoints[indexPreviousPoint].Y;

                    indexPreviousPoint = indexCurrentPoint;
                }
            }
            else
            { // We have a full array of points.
                indexPreviousPoint = indexToNextInsertPosition;  // indexToNextInsertPosition is the oldest tracked point.

                for (int i = 1; i < NR_POINTS_TO_TRACK ; ++i)
                {
                    indexCurrentPoint = (indexPreviousPoint + 1) % NR_POINTS_TO_TRACK;

                    deltaX += trackedPoints[indexCurrentPoint].X - trackedPoints[indexPreviousPoint].X;
                    deltaY += trackedPoints[indexCurrentPoint].Y - trackedPoints[indexPreviousPoint].Y;

                    indexPreviousPoint = indexCurrentPoint;
                }

            }

            return new Point(deltaX, deltaY);
        }
    }

    /// <summary>
    /// The ProtonCollisionManager is responsible for detecting and animating collisions.
    /// </summary>
    class ProtonCollisionManager
    {
        private ProtonCollisionDetector collisionDetector;
        private ProtonCollisionAnimator collisionAnimator;
        private DispatcherTimer detectionTimer;
        private const int CHECKING_FREQUENCY = 20;  // x per seconds [Hz]

        public ProtonCollisionManager()
        {
            collisionDetector = new ProtonCollisionDetector(this);
            collisionAnimator = new ProtonCollisionAnimator(this);
            detectionTimer = new DispatcherTimer();
            detectionTimer.Interval = TimeSpan.FromSeconds(1.0 / CHECKING_FREQUENCY);  // [ms]

            detectionTimer.Tick += (s, args) =>
            {
                collisionDetector.hitTest();
                collisionDetector.destroyProtonsOutsideOfTheScreen();

            };
            
        }


        public void startDetection()
        {
            detectionTimer.Start();
        }
        public void stopDetection()
        {
            detectionTimer.Stop();
        }

        public void addProton(Proton proton)
        {
            collisionDetector.addProton(proton);
        }

        protected void detectedCollisionBetween(Proton proton_one, Proton proton_two)
        {
            int collisionPoints = ProtonCollisionPointsCalculator.calculateCollisionPointsOf(proton_one, proton_two);
            collisionAnimator.animateCollision(proton_one, proton_two);            
        }

        /// <summary>
        /// The ProtonCollisionDetector supports the ProtonCollisionManager and detects collisions.
        /// </summary>
        class ProtonCollisionDetector
        {
            private ProtonCollisionManager collisionManager;
            private List<Proton> protons;
            private int protonsInMovingState = 0;

            private double upperBorder;
            private double lowerBorder;
            private double leftBorder;
            private double rightBorder;

            public ProtonCollisionDetector(ProtonCollisionManager manager)
            {
                collisionManager = manager;
                protons = new List<Proton>();

                // TODO: calculate borders dynamically.
                upperBorder = 0 - Proton.PROTON_RADIUS;
                lowerBorder = 1920 + Proton.PROTON_RADIUS;
                leftBorder = 0 - Proton.PROTON_RADIUS;
                rightBorder = 1080 + Proton.PROTON_RADIUS;
            }


            public void addProton(Proton proton)
            {
                protons.Add(proton);
                //TODO: listen on notifications from proton
            }

            public void hitTest()
            {
                //if (protonsInMovingState == 0)
                //{ // No proton is moving
                //    return;
                //}

                for (int i = 0; i < protons.Count-1; ++i)
                {
                    for (int j = i+1; j < protons.Count; ++j)
                    {
                        if (hitEachother(protons[i], protons[j]))
                        {
                            collisionManager.detectedCollisionBetween(protons[i], protons[j]);
                        }
                    }
                }
            }

            private bool hitEachother(Proton proton_one, Proton proton_two)
            {
                // Two protons hit each other if the distance between the both centers
                // is smaller than the diagonal of a proton.
                // For simplicity and to save calculations we test only the distance in X and Y.
                double d = 2 * Proton.PROTON_RADIUS;

                if (Math.Abs(proton_one.ActualCenter.X - proton_two.ActualCenter.X) < d)
                    if (Math.Abs(proton_one.ActualCenter.Y - proton_two.ActualCenter.Y) < d)
                        return true;
                return false;
            }

            internal void destroyProtonsOutsideOfTheScreen()
            {
                foreach (Proton proton in protons)
                {
                    if (proton.isMoving())
                    {
                        if (proton.ActualCenter.X < upperBorder || proton.ActualCenter.X > lowerBorder ||
                            proton.ActualCenter.Y < leftBorder || proton.ActualCenter.Y > rightBorder)
                        {

                            proton.destroy();
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Responsible for the animation of the collision of two protons.
        /// </summary>
        class ProtonCollisionAnimator
        {
            private ProtonCollisionManager protonCollisionManager;

            public ProtonCollisionAnimator(ProtonCollisionManager protonCollisionManager)
            {
                this.protonCollisionManager = protonCollisionManager;
            }

            public void animateCollision(Proton proton_one, Proton proton_two)
            {
                Point collisionPoint =  getCollisionPoint(proton_one, proton_two);
                
                //TODO: @phineliner start animation on Point 
                Console.WriteLine("COLLSION_COLLISION_COLLSION_COLLISION_COLLSION_COLLISION_COLLSION_COLLISION_COLLSION_COLLISION");
                
                proton_one.destroy();
                proton_two.destroy();
            }

            private Point getCollisionPoint(Proton proton_one, Proton proton_two)
            {
                // x = (x_1 + x_2) / 2
                double x_pos = (proton_one.ActualCenter.X + proton_two.ActualCenter.X) / 2;
                // y = (y_1 + y_2) / 2
                double y_pos = (proton_one.ActualCenter.Y + proton_two.ActualCenter.Y) / 2;

                return new Point(x_pos, y_pos);
            }

        }


        /// <summary>
        /// Responsible for calculating the collision points
        /// </summary>
        class ProtonCollisionPointsCalculator
        {

            public static int calculateCollisionPointsOf(Proton proton_one, Proton proton_two)
            {
                // Of course the following calculation of the collision points is not physically correct 
                // but includes the speed and the rotation of the protons
                double v1 = proton_one.getVelocityEnergy();
                double r1 = proton_one.getRotationEnergy();

                double v2 = proton_two.getVelocityEnergy();
                double r2 = proton_two.getRotationEnergy();

                double collisionpoints = 50 * r1 + 50 * r2+ v1 / 1000 + v2 / 1000;

                return (int)collisionpoints;
            }
        }
    }
}
