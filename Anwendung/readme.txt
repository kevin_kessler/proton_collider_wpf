Dieser Ordner beinhaltet die ausf�hrbaren Anwendungen sowie die daf�r ben�tigten Ressourcen

* \InfotableContent: Ressourcen f�r die Infotafel-Anwendung. Der Inhalt des Ordners muss vor der Ausf�hrung nach "C:\Infotable\" kopiert werden

* \InfotableApp: Beinhaltet die Ausf�hrbare Infotafel Anwendung (Infotable.exe)

* \ProtonColliderApp: Beinhaltet die ausf�hrbare Proton Collider Anwendung (ProtonCollider.exe). Zur Ausf�hrung muss der Unterordner "Resources" im selben Ordner wie die ProtonCollider.exe liegen