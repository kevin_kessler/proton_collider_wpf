﻿using System;

namespace Infotable.mvvm.locator
{
    public class LocatorAttribute : Attribute
    {
        public string Name { get; set; }

        public LocatorAttribute(string name)
        {
            Name = name;
        }
    }
}
