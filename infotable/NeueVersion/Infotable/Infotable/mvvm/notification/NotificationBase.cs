﻿using System;
using System.ComponentModel;

namespace Infotable.mvvm.notification
{
    /// <summary>
    /// the base class to pass a notificate between View and ViewModel
    /// </summary>
    public class NotificationBase : INotifyPropertyChanged
    {
        /// <summary>
        /// this methode checks if the event was subscribed and fires it
        /// </summary>
        /// <param name="propertyName"></param>
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
