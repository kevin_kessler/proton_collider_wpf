﻿using Infotable.model.enums;
using Infotable.mvvm.commanding;
using Infotable.mvvm.model;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Infotable.model
{
    public class Datei : ModelBase
    {
        #region Members

        private string pfad;
        private string verzeichnis;
        private string name;

        private Data data;

        // Attribute für ein Bild
        private BitmapImage image;
        
        // Attribute für ein Video
        private MediaState mediaState;
        private Visibility playButton;
        private Visibility pauseButton;

        #endregion

        #region Constructors

        public Datei()
        {
            this.Clear();
        }

        public Datei(string pfad, string verzeichnis, string name, Data data)
        {
            this.pfad = pfad;
            this.verzeichnis = verzeichnis;
            this.name = name;

            this.data = data;

            if (data == enums.Data.Bild)
            {
                BitmapImage img = new BitmapImage();
                try
                {
                    img.BeginInit();
                    img.CacheOption = BitmapCacheOption.OnLoad;
                    img.UriSource = new Uri(pfad);
                    img.EndInit();
                }
                catch { }

                this.image = img;

                this.mediaState = MediaState.Close;
                this.playButton = Visibility.Collapsed;
                this.pauseButton = Visibility.Collapsed;
            }
            else
            {
                this.image = null;

                this.mediaState = MediaState.Stop;
                this.playButton = Visibility.Visible;
                this.pauseButton = Visibility.Collapsed;
            }
        }

        public Datei(Datei d)
        {
            this.pfad = d.pfad;
            this.verzeichnis = d.verzeichnis;
            this.name = d.name;

            this.data = d.data;

            this.image = d.image;

            this.mediaState = d.mediaState;
            this.playButton = d.playButton;
            this.pauseButton = d.pauseButton;
        }

        #endregion

        #region Properties

        public string Pfad
        {
            get { return pfad; }
            set
            {
                if (pfad == value)
                    return;
                pfad = value;
                RaisePropertyChanged("Pfad");
            }
        }

        public string Verzeichnis
        {
            get { return verzeichnis; }
            set
            {
                if (verzeichnis == value)
                    return;
                verzeichnis = value;
                RaisePropertyChanged("Verzeichnis");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    return;
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public Data Data
        {
            get { return data; }
            set
            {
                if (data == value)
                    return;
                data = value;
                RaisePropertyChanged("Data");
            }
        }

        public BitmapImage Image
        {
            get { return image; }
            set
            {
                if (image == value)
                    return;
                image = value;
                RaisePropertyChanged("Image");
            }
        }

        public MediaState MediaState
        {
            get { return mediaState; }
            set
            {
                if (mediaState == value)
                    return;
                mediaState = value;
                RaisePropertyChanged("MediaState");

                if(value == System.Windows.Controls.MediaState.Play)
                {
                    this.PlayButton = Visibility.Collapsed;
                    this.PauseButton = Visibility.Visible;
                }
                else if (value == System.Windows.Controls.MediaState.Pause || value == System.Windows.Controls.MediaState.Stop)
                {
                    this.PlayButton = Visibility.Visible;
                    this.PauseButton = Visibility.Collapsed;
                }
            }
        }

        public Visibility PlayButton
        {
            get { return playButton; }
            set
            {
                if (playButton == value)
                    return;
                playButton = value;
                RaisePropertyChanged("PlayButton");
            }
        }

        public Visibility PauseButton
        {
            get { return pauseButton; }
            set
            {
                if (pauseButton == value)
                    return;
                pauseButton = value;
                RaisePropertyChanged("PauseButton");
            }
        }

        #endregion

        #region Commands

        #region PlayVideoCommand

        private RelayCommand playVideoCommand;

        public ICommand PlayVideoCommand
        {
            get
            {
                if (this.playVideoCommand == null)
                    this.playVideoCommand = new RelayCommand(PlayVideo);
                return this.playVideoCommand;
            }
        }

        private void PlayVideo()
        {
            if (this.Data == enums.Data.Video)
            {
                if (this.MediaState == System.Windows.Controls.MediaState.Stop)
                    this.MediaState = System.Windows.Controls.MediaState.Play;
                else if (this.MediaState == System.Windows.Controls.MediaState.Pause)
                    this.MediaState = System.Windows.Controls.MediaState.Play;
                else if (this.MediaState == System.Windows.Controls.MediaState.Play)
                    this.MediaState = System.Windows.Controls.MediaState.Pause;
            }
        }

        #endregion

        #region MediaEndedCommand

        private RelayCommand mediaEndedCommand;

        public ICommand MediaEndedCommand
        {
            get
            {
                if (this.mediaEndedCommand == null)
                    this.mediaEndedCommand = new RelayCommand(MediaEnded);
                return this.mediaEndedCommand;
            }
        }

        private void MediaEnded()
        {
            if (this.Data == enums.Data.Video)
                this.MediaState = System.Windows.Controls.MediaState.Stop;
        }

        #endregion

        #endregion

        #region Methods

        public void Clear()
        {
            this.pfad = string.Empty;
            this.verzeichnis = string.Empty;
            this.name = string.Empty;

            this.data = enums.Data.Empty;

            this.image = null;

            this.mediaState = MediaState.Stop;
            this.playButton = Visibility.Visible;
            this.pauseButton = Visibility.Collapsed;
        }

        #endregion
    }
}
