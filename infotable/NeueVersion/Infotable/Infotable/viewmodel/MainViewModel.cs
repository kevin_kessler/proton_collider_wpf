﻿using FluidKit.Controls;
using Infotable.model;
using Infotable.mvvm.commanding;
using Infotable.mvvm.locator;
using Infotable.mvvm.viewmodel;
using Infotable.service;
using Infotable.view;
using Microsoft.Surface.Presentation.Controls;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Infotable.viewmodel
{
    [LocatorAttribute(ViewModelNames.MAIN_VIEW_NAME)]
    public class MainViewModel : ViewModelBase
    {
        #region Members

        private ObservableCollection<Person> personen;
        private Person person;
        private bool showPerson;

        private ObservableCollection<News> neuigkeiten;
        private News neuigkeit;
        private bool showNeuigkeiten;

        private NewsTicker newsTicker;

        private Visibility showHelp;

        private DispatcherTimer updateTimer;

        #endregion

        #region Constructors

        public MainViewModel()
        {
            this.Clear();

            XML.SetDirectory();

            this.LoadData();

            this.ShowNeuigkeiten = false;
            this.ShowHelp = Visibility.Collapsed;

            this.updateTimer = new DispatcherTimer();
            this.updateTimer.Interval = new TimeSpan(1, 0, 0); // 1h
            this.updateTimer.Tick += updateTimer_Tick;
            this.updateTimer.Start();
        }

        #endregion

        #region Properties

        public ObservableCollection<Person> Personen
        {
            get { return personen; }
            set
            {
                if (personen == value)
                    return;
                personen = value;
                RaisePropertyChanged("Personen");
            }
        }

        public Person Person
        {
            get { return person; }
            set
            {
                if (person == value)
                    return;
                person = value;
                RaisePropertyChanged("Person");

                if (value != null)
                {
                    this.ShowPerson = true;
                    this.ShowNeuigkeiten = false;

                    // setze MediaState aller Videos auf "STOP"
                    for (int i = 0; i < this.Person.VideoPfade.Count; i++)
                    {
                        this.Person.VideoPfade[i].MediaState = System.Windows.Controls.MediaState.Stop;
                    }
                }
                else
                    this.ShowPerson = false;
            }
        }

        public bool ShowPerson
        {
            get { return showPerson; }
            set
            {
                if (showPerson == value)
                    return;
                showPerson = value;
                RaisePropertyChanged("ShowPerson");
            }
        }

        public ObservableCollection<News> Neuigkeiten
        {
            get { return neuigkeiten; }
            set
            {
                if (neuigkeiten == value)
                    return;
                neuigkeiten = value;
                RaisePropertyChanged("Neuigkeiten");

                if (value != null)
                    this.NewsTicker = this.GetNewsTicker();
            }
        }

        public News Neuigkeit
        {
            get { return neuigkeit; }
            set
            {
                if (neuigkeit == value)
                    return;
                neuigkeit = value;
                RaisePropertyChanged("Neuigkeit");
            }
        }

        public bool ShowNeuigkeiten
        {
            get { return showNeuigkeiten; }
            set
            {
                if (showNeuigkeiten == value)
                    return;
                showNeuigkeiten = value;
                RaisePropertyChanged("ShowNeuigkeiten");
            }
        }

        public NewsTicker NewsTicker
        {
            get { return newsTicker; }
            set
            {
                if (newsTicker == value)
                    return;
                newsTicker = value;
                RaisePropertyChanged("NewsTicker");
            }
        }

        public Visibility ShowHelp
        {
            get { return showHelp; }
            set
            {
                if (showHelp == value)
                    return;
                showHelp = value;
                RaisePropertyChanged("ShowHelp");
            }
        }

        #endregion

        #region Commands

        #region HelpCommand

        private RelayCommand helpCommand;

        public ICommand HelpCommand
        {
            get
            {
                if (this.helpCommand == null)
                    this.helpCommand = new RelayCommand(Help);
                return this.helpCommand;
            }
        }

        private void Help()
        {
            if (this.ShowHelp == Visibility.Collapsed)
                this.ShowHelp = Visibility.Visible;
            else
                this.ShowHelp = Visibility.Collapsed;
        }

        #endregion

        #region CloseHelpUserControl

        private RelayCommand closeHelpUserControl;

        public ICommand CloseHelpUserControl
        {
            get
            {
                if (this.closeHelpUserControl == null)
                    this.closeHelpUserControl = new RelayCommand(CloseHelp);
                return this.closeHelpUserControl;
            }
        }

        private void CloseHelp()
        {
            this.ShowHelp = Visibility.Collapsed;
        }

        #endregion

        #region ShowNewsCommand

        private RelayCommand showNewsCommand;

        public ICommand ShowNewsCommand
        {
            get
            {
                if (this.showNewsCommand == null)
                    this.showNewsCommand = new RelayCommand(ShowNews);
                return this.showNewsCommand;
            }
        }

        private void ShowNews()
        {
            if (this.ShowNeuigkeiten)
            {
                this.ShowPerson = true;
                this.ShowNeuigkeiten = false;
            }
            else
            {
                this.ShowPerson = false;
                this.ShowNeuigkeiten = true;
            }
        }

        #endregion

        #region CloseNewsUserControl

        private RelayCommand closeNewsUserControl;

        public ICommand CloseNewsUserControl
        {
            get
            {
                if (this.closeNewsUserControl == null)
                    this.closeNewsUserControl = new RelayCommand(CloseNews);
                return this.closeNewsUserControl;
            }
        }

        private void CloseNews()
        {
            this.ShowNeuigkeiten = false;

            if (this.Person != null)
                this.ShowPerson = true;
        }

        #endregion

        #region ChangeTimeLineCommand

        private RelayCommand<SurfaceWindow> changeTimeLineCommand;

        public ICommand ChangeTimeLineCommand
        {
            get
            {
                if (this.changeTimeLineCommand == null)
                    this.changeTimeLineCommand = new RelayCommand<SurfaceWindow>(ChangeTimeLine);
                return this.changeTimeLineCommand;
            }
        }
        private int counter = 0;
        private void ChangeTimeLine(SurfaceWindow win)
        {
            LayoutBase[] layouts = {   new CoverFlow(),
                                       new VForm(),
		                               //new SlideDeck(),
		                               new Wall(),
		                               new Carousel(),
                                       //new TimeMachine2(),
                                       //new ThreeLane(),
		                               new TimeMachine(),
		                               //new RollerCoaster(),
		                               //new Rolodex(),
		                                };

            ElementFlow timeline = ((MainView)win).TimeLine;

            timeline.Layout = layouts[++counter % layouts.Length];
        }

        #endregion

        #endregion

        #region Events

        /// <summary>
        /// timer zum aktualiseren der daten
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void updateTimer_Tick(object sender, EventArgs e)
        {
            this.LoadData();
        }

        #endregion

        #region Methods

        private void Clear()
        {
            this.Personen = new ObservableCollection<Person>();
            this.Person = null;
            this.ShowPerson = false;
            this.Neuigkeiten = new ObservableCollection<News>();
            this.Neuigkeit = null;
            this.ShowNeuigkeiten = false;
            this.NewsTicker = new NewsTicker();
            this.ShowHelp = Visibility.Collapsed;
        }

        /// <summary>
        /// calculates the NewsTicker Properties
        /// </summary>
        /// <returns>the NewsTicker to the current News</returns>
        private NewsTicker GetNewsTicker()
        {
            int letterSize = 48;
            int windowSize = (int)(App.Current.MainWindow.Width);

            NewsTicker result = new NewsTicker();

            string content = string.Empty;
            for (int i = 0; i < this.Neuigkeiten.Count; i++)
            {
                if (i == (this.Neuigkeiten.Count - 1))
                    content += this.Neuigkeiten[i].Titel + " - " + this.Neuigkeiten[i].Inhalt;
                else
                    content += this.Neuigkeiten[i].Titel + " - " + this.Neuigkeiten[i].Inhalt + "     +++++     +++++     ";
            }
            result.Content = content;

            result.Width = letterSize * content.Length;

            result.From = windowSize;
            result.To = (int)((windowSize - result.Width) * 0.675);

            result.Duration = new Duration(new TimeSpan(0,0,content.Length/7));

            return result;
        }

        /// <summary>
        /// lade Personen und Neuigkeiten neu
        /// </summary>
        private void LoadData()
        {
            if (Directory.Exists(XML.verzeichnis))
            {
                // lade personen
                ObservableCollection<Person> temp = XML.getPersonen();
                if (this.Personen.Count == 0)
                {
                    // bei app start
                    this.Personen = temp;
                }
                else
                {
                    // für aktualisierung nach jedem intervall
                    this.Personen.Clear();
                    for (int i = 0; i < temp.Count; i++)
                        this.Personen.Add(temp[i]);
                }
                if (this.Personen.Count > 0)
                    this.Person = this.Personen[(this.Personen.Count % 2 == 0) ? ((this.Personen.Count / 2) - 1) : (this.Personen.Count / 2)];
                else
                    this.Person = null;

                // lade neuigkeiten
                this.Neuigkeiten = XML.getNeuigkeiten();
                this.Neuigkeit = null;
            }
        }

        #endregion
    }
}
