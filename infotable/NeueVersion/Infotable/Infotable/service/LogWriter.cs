﻿using System;
using System.IO;

namespace Infotable.service.log
{
    public static class LogWriter
    {
        #region Constants

        public const String DATE_FORMAT = "yyyy_MM_dd";
        private const String LOG_SUFFIX = "_EXC";
        private const String LOG_EXTENSION = ".log";
        public const String MSG_SUFFIX = "_MSG";
        public const String MSG_EXTENSION = ".txt";

        #endregion

        #region Methodes

        /// <summary>
        /// writes a message in a log-file
        /// </summary>
        /// <param name="message">The message to write in the log</param>
        public static void writeLog(string message)
        {
            try
            {
                //the base directory
                string _baseDirectory = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.RelativeSearchPath + "Log/";

                //if the "Log" directory not exists, then create it
                if (!Directory.Exists(_baseDirectory))
                {
                    DirectoryInfo di = Directory.CreateDirectory(_baseDirectory);
                }

                string _filename = _baseDirectory + GetFilenameYYYMMDD(LogWriter.LOG_SUFFIX, LogWriter.LOG_EXTENSION);

                //create new streamwriter to write the message in the log-file
                StreamWriter sw = new StreamWriter(_filename, true);
                sw.WriteLine("[" + System.DateTime.Now.ToString() + "] : " + message);
                sw.Close();
            }
            catch (Exception ex)
            {
                writeLog(ex);
            }
        }

        /// <summary>
        /// writes a message in a file .txt file
        /// </summary>
        /// <param name="message">the message</param>
        /// <param name="path">the path were the file will be writed + /Messages/</param>
        public static void writeLog(string message, string path)
        {
            try
            {
                //the base directory
                string directory = path + "\\Messages\\";

                //if the "Log" directory not exists, then create it
                if (!Directory.Exists(directory))
                {
                    DirectoryInfo di = Directory.CreateDirectory(directory);
                }

                string _filename = directory + GetFilenameYYYMMDD(LogWriter.MSG_SUFFIX, LogWriter.MSG_EXTENSION);

                //create new streamwriter to write the message in the file
                StreamWriter sw = new StreamWriter(_filename, true);
                sw.WriteLine("[" + System.DateTime.Now.ToString() + "] : " + message);
                sw.Close();

            }
            catch (Exception ex)
            {
                writeLog(ex);
            }
        }

        /// <summary>
        /// writes an exception in a log-file
        /// </summary>
        /// <param name="exception">The exception to write in the log</param>
        public static void writeLog(Exception exception)
        {
            try
            {
                //the base directory
                string _baseDirectory = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.RelativeSearchPath + "Log/";

                //if the "Log" directory not exists, then create it
                if (!Directory.Exists(_baseDirectory))
                {
                    DirectoryInfo di = Directory.CreateDirectory(_baseDirectory);
                }

                string _filename = _baseDirectory + GetFilenameYYYMMDD(LogWriter.LOG_SUFFIX, LogWriter.LOG_EXTENSION);

                //create new streamwriter to write the message in the log-file
                StreamWriter sw = new StreamWriter(_filename, true);
                sw.WriteLine("[" + System.DateTime.Now.ToString() + "] - Exception : " + exception.Source);
                sw.WriteLine("\t\t" + exception.Message);
                sw.Close();
            }
            catch (Exception ex)
            {
                writeLog(ex);
            }
        }

        /// <summary>
        /// Writes a exception with a message into a log-file
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="exception">The exception</param>
        public static void writeLog(string message, Exception exception)
        {
            try
            {
                //the base directory
                string _baseDirectory = AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.RelativeSearchPath + "Log/";

                //if the "Log" directory not exists, then create it
                if (!Directory.Exists(_baseDirectory))
                {
                    DirectoryInfo di = Directory.CreateDirectory(_baseDirectory);
                }

                string _filename = _baseDirectory + GetFilenameYYYMMDD(LogWriter.LOG_SUFFIX, LogWriter.LOG_EXTENSION);

                //create new streamwriter to write the message in the log-file
                StreamWriter sw = new StreamWriter(_filename, true);
                sw.WriteLine("[" + System.DateTime.Now.ToString() + "] - Exception : " + exception.Source);
                sw.WriteLine(message);
                sw.WriteLine("\t\t" + exception.Message);
                sw.Close();
            }
            catch (Exception ex)
            {
                writeLog(ex);
            }
        }

        /// <summary>
        /// Generates the file name of the log (yyyy_MM_dd + suffix + extension).
        /// </summary>
        /// <param name="suffix">The suffix of the file name</param>
        /// <param name="extension">The extension of the file name</param>
        /// <returns>The file name</returns>
        public static string GetFilenameYYYMMDD(string suffix, string extension)
        {
            return System.DateTime.Now.ToString(LogWriter.DATE_FORMAT) + suffix + extension;
        }

        #endregion
    }
}
