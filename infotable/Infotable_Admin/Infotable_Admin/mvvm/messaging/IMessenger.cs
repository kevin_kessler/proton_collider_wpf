﻿using System;

namespace Infotable_Admin.mvvm.messaging
{
    interface IMessenger
    {
        void Register(object recipient, string identCode, Action action);
        void Register<TNotification>(object recipient, Action<TNotification> action);
        void Register<TNotification>(object recipient, string identCode, Action<TNotification> action);

        void Send(string identCode);
        void Send<TNotification>(TNotification notification);
        void Send<TNotification>(TNotification notification, string identCode);

        void Unregister(string identCode);
        void Unregister<TNotification>(object recipient);
        void Unregister<TNotification>(object recipient, string identCode);
    }
}
