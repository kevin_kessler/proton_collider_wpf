﻿
namespace Infotable_Admin.mvvm.messaging
{
    public class ActionIdentifier
    {
        public WeakReferenceAction Action { get; set; }
        public string IdentificationCode { get; set; }
    }
}
