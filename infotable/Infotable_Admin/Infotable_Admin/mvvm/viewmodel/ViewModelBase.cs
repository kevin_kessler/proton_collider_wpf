﻿using Infotable_Admin.mvvm.notification;

namespace Infotable_Admin.mvvm.viewmodel
{
    /// <summary>
    /// the base class for the viewmodels
    /// </summary>
    public class ViewModelBase : NotificationBase
    {
    }
}
