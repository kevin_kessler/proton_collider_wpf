﻿using System;
using System.ComponentModel;
using Infotable_Admin.mvvm.notification;

namespace Infotable_Admin.mvvm.model
{
    /// <summary>
    /// this class is for validating the data and gives the user a hint of an error
    /// </summary>
    public class ModelBase : NotificationBase, IDataErrorInfo
    {
        public virtual string Error
        {
            get { return String.Empty; }
        }

        public virtual string this[string columnName]
        {
            get { return String.Empty; }
        }
    }
}
