﻿using Infotable_Admin.mvvm.commanding;
using Infotable_Admin.mvvm.locator;
using Infotable_Admin.mvvm.viewmodel;
using Infotable_Admin.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Infotable_Admin.viewmodel
{
    [LocatorAttribute(ViewModelNames.LOGIN_VIEW_NAME)]
    public class LoginViewModel : ViewModelBase
    {
        #region Members

        #endregion

        #region Constructor

        #endregion

        #region Properties

        #endregion

        #region Commands

        #region CancelCommand

        private RelayCommand<Window> cancelCommand;

        public ICommand CancelCommand
        {
            get
            {
                if (this.cancelCommand == null)
                    this.cancelCommand = new RelayCommand<Window>(Cancel);
                return this.cancelCommand;
            }
        }

        private void Cancel(Window win)
        {
            win.Close();
        }

        #endregion

        #region LoginCommand

        private RelayCommand<Window> loginCommand;

        public ICommand LoginCommand
        {
            get
            {
                if (this.loginCommand == null)
                    this.loginCommand = new RelayCommand<Window>(Login);
                return this.loginCommand;
            }
        }

        private void Login(Window win)
        {
            string pw = ((LoginView)win).pwBox.Password;

            if (pw.Equals("test"))
            {
                MainView mv = new MainView();
                mv.Show();
                win.Close();       
            }
            else
            {
                ((LoginView)win).pwBox.Password = string.Empty;
                MessageBox.Show("Falsches Passwort!");
            }
        }

        #endregion

        #endregion

        #region Methods

        #endregion
    }
}
