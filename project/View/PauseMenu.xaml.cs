﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProtonCollider.View
{
       
    /// <summary>
    /// Interaktionslogik für PauseMenu.xaml
    /// </summary>
    public partial class PauseMenu : UserControl
    {

        // Delegates fuer Resume, Restart und BackToMain Methoden
        public delegate void PauseHandler();
        private PauseHandler OnResume;
        private PauseHandler OnRestart;
        private PauseHandler OnBackToMain;

        /// <summary>
        /// Gibt an ob das PauseMenu zur Zeit aktiv oder inaktiv ist
        /// </summary>
        public bool IsActive = false;

        /// <summary>
        /// Konstruktor des Pausemenues.
        /// Weist beim Erzeugen den Buttons die entsprechenden Funktionen zu
        /// </summary>
        public PauseMenu(PauseHandler onResume, PauseHandler onRestart, PauseHandler onBackToMain)
        {
            InitializeComponent();

            OnResume = onResume;
            OnRestart = onRestart;
            OnBackToMain = onBackToMain;
        }

        /// <summary>
        /// Ruf das OnResume() delegate auf.
        /// Wird aufgerufen sobald der Resume Button geklickt / getoucht wurde.
        /// </summary>
        private void ResumeButton_Click(object sender, RoutedEventArgs e)
        {
            OnResume();
        }

        /// <summary>
        /// Ruf das OnRestart() delegate auf.
        /// Wird aufgerufen sobald der Restart Button geklickt / getoucht wurde.
        /// </summary>
        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            OnRestart();
        }

        /// <summary>
        /// Ruf das OnBackToMain() delegate auf.
        /// Wird aufgerufen sobald der BackToMain Button geklickt / getoucht wurde.
        /// </summary>
        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            OnBackToMain();
        }


    }
}
