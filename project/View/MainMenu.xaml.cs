﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProtonCollider.Controller;
using ProtonCollider.Model;

namespace ProtonCollider
{
    /// <summary>
    /// Interaktionslogik für MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Laedt die Gameview und initialisiert das Level
        /// </summary>
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            ViewManager.Instance.ChangeView(ViewManager.Views.GameScreen);
            GameManager.Instance.LoadLevel(new Level(60));
        }

        /// <summary>
        /// Laedt die Highscoreview und aktualisiert deren Ausgabe mit den aktuellen Highscore Eintraegen
        /// </summary>
        private void HighscoreButton_Click(object sender, RoutedEventArgs e)
        {
            ViewManager.Instance.ChangeView(ViewManager.Views.HighScore);
            ViewManager.Instance.ScoreView.UpdateHighScoreList(SaveManager.Instance.HighScore);
        }

        /// <summary>
        /// Beendet die Anwendung
        /// </summary>
        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.Close();
        }

        /// <summary>
        /// Laedt die Infoview
        /// </summary>
        private void InfoButton_Click(object sender, RoutedEventArgs e)
        {
            ViewManager.Instance.ChangeView(ViewManager.Views.InfoScreen);
        }
    }
}
