using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using ProtonCollider.View;
using ProtonCollider.Controller;

namespace ProtonCollider
{
    /// <summary>
    /// Interaktionslogik fuer das Hauptfenster, welches als Geruest fuer die einzelnen Views dient.
    /// Ist ausserdem fuer das Darstellen und Verwalten des Hintergrundvideos zustaendig, da dieses
    /// in allen Views zum Einsatz kommt.
    /// </summary>
    public partial class MainWindow : SurfaceWindow
    {
        //Die Anwendung wird auf Basis der FullHD Aufloesung designed.
        //Die GUI-Elemente werden aber beim Oeffnen der Anwendung auf die
        //Aktuelle Aufloesung skaliert (Responsive Design / Aufloesungsunabhaengigkeit). 
        //Wichtig ist nur das Seitenverhaeltnis 16:9
        public const float DESIGNWIDTH = 1920;
        public const float DESIGNHEIGHT = 1080;

        /// <summary>
        /// Singletion Instance
        /// </summary>
        private static MainWindow instance;
        public static MainWindow Instance
        {
            get
            {
                if (null == instance)
                    instance = new MainWindow();
                return instance;
            }

            private set{}
        }

        // Background Video
        private const string _BG_VIDEO_SRC = @"Resources/bg.wmv";
        private MediaElement bgVideo;

        /// <summary>
        /// Default constructor.
        /// </summary>
        private MainWindow()
        {
            InitializeComponent();

            InitBGVideo();

            // Add handlers for window availability events
            AddWindowAvailabilityHandlers();
        }

        // Die folgende region umfasst alle Skalierungsmethoden welche fuer das Responsive Design / die Aufloesungsunabhaengigkeit benoetigt werden
        // Quelle: http://stackoverflow.com/questions/3193339/tips-on-developing-resolution-independent-application
        #region ScaleValue Depdency Property
        
        public static readonly DependencyProperty ScaleValueProperty = DependencyProperty.Register("ScaleValue", typeof(double), typeof(MainWindow), new UIPropertyMetadata(1.0, new PropertyChangedCallback(OnScaleValueChanged), new CoerceValueCallback(OnCoerceScaleValue)));


        private static object OnCoerceScaleValue(DependencyObject o, object value)
        {
            MainWindow mainWindow = o as MainWindow;
            if (mainWindow != null)
                return mainWindow.OnCoerceScaleValue((double)value);
            else
                return value;
        }

        private static void OnScaleValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            MainWindow mainWindow = o as MainWindow;
            if (mainWindow != null)
                mainWindow.OnScaleValueChanged((double)e.OldValue, (double)e.NewValue);
        }

        protected virtual double OnCoerceScaleValue(double value)
        {
            if (double.IsNaN(value))
                return 1.0f;

            value = Math.Max(0.1, value);
            return value;
        }

        protected virtual void OnScaleValueChanged(double oldValue, double newValue)
        {
        }

        public double ScaleValue
        {
            get
            {
                return (double)GetValue(ScaleValueProperty);
            }
            set
            {
                SetValue(ScaleValueProperty, value);
            }
        }

        /// <summary>
        /// Skaliert die Inhalte des Windows, wenn dieses seine Groese veraendert
        /// </summary>
        private void MainGrid_SizeChanged(object sender, EventArgs e)
        {
            CalculateScale();
        }

        /// <summary>
        /// Berechnet die ScaleValue in Abhaenigkeit der Aktuellen Aufloesung zur Designaufloesung
        /// und skaliert die Inhalte entsprechend
        /// </summary>
        private void CalculateScale()
        {
            double yScale = ActualHeight / DESIGNHEIGHT;
            double xScale = ActualWidth / DESIGNWIDTH;
            double value = Math.Min(xScale, yScale);
            ScaleValue = (double)OnCoerceScaleValue(myMainWindow, value);
        }
        #endregion

        #region Default Handlers

        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Remove handlers for window availability events
            RemoveWindowAvailabilityHandlers();
        }

        /// <summary>
        /// Adds handlers for window availability events.
        /// </summary>
        private void AddWindowAvailabilityHandlers()
        {
            // Subscribe to surface window availability events
            ApplicationServices.WindowInteractive += OnWindowInteractive;
            ApplicationServices.WindowNoninteractive += OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable += OnWindowUnavailable;
        }

        /// <summary>
        /// Removes handlers for window availability events.
        /// </summary>
        private void RemoveWindowAvailabilityHandlers()
        {
            // Unsubscribe from surface window availability events
            ApplicationServices.WindowInteractive -= OnWindowInteractive;
            ApplicationServices.WindowNoninteractive -= OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable -= OnWindowUnavailable;
        }

        /// <summary>
        /// This is called when the user can interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowInteractive(object sender, EventArgs e)
        {
            //TODO: enable audio, animations here
        }

        /// <summary>
        /// This is called when the user can see but not interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowNoninteractive(object sender, EventArgs e)
        {
            //TODO: Disable audio here if it is enabled

            //TODO: optionally enable animations here
        }

        /// <summary>
        /// This is called when the application's window is not visible or interactive.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowUnavailable(object sender, EventArgs e)
        {
            //TODO: disable audio, animations here
        }

        #endregion
        
        /// <summary>
        /// Initialisiert das Hintergrundvideo, setzt es auf den ersten Frame
        /// und zeigt das Video an
        /// </summary>
        private void InitBGVideo()
        {
            // Create a MediaElement object.
            bgVideo = new MediaElement();
            bgVideo.Source = new Uri(_BG_VIDEO_SRC, UriKind.Relative);
            bgVideo.LoadedBehavior = MediaState.Manual;
            bgVideo.UnloadedBehavior = MediaState.Manual;
            bgVideo.ScrubbingEnabled = true; //needed to enable manually set frames

            // set video to first frame when loaded
            bgVideo.Loaded += new RoutedEventHandler(this.ShowFirstFrame);

            // loop the bg video forever
            bgVideo.MediaEnded += new RoutedEventHandler(this.ReplayBGVideo);
            
            // draw background video
            VisualBrush vb = new VisualBrush();
            vb.Visual = bgVideo;
            MainGrid.Background = vb;
        }

        /// <summary>
        /// Setzt das Hintergrundvideo auf den ersten Frame
        /// </summary>
        private void ShowFirstFrame(object sender, EventArgs e)
        {
            bgVideo.Position = TimeSpan.FromSeconds(0);
            bgVideo.Play();
            bgVideo.Pause();
        }

        /// <summary>
        /// Startet das Hintergrundvideo von vorne
        /// </summary>
        private void ReplayBGVideo(object sender, EventArgs e)
        {
            bgVideo.Position = TimeSpan.FromSeconds(0);
            bgVideo.Play();
        }

        /// <summary>
        /// Pausiert das Hintergrundvideo
        /// </summary>
        public void PauseBGVideo()
        {
            bgVideo.Pause();
        }

        /// <summary>
        /// Setzt die Wiedergabe des Hintergrundvideos fort
        /// </summary>
        public void ResumeBGVideo()
        {
            bgVideo.Play();
        }

    }
}