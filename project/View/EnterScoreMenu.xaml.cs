﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProtonCollider.Controller;
using ProtonCollider.Model;
using System.Collections.ObjectModel;

namespace ProtonCollider.View
{
    /// <summary>
    /// Interaktionslogik für EnterScoreMenu.xaml
    /// </summary>
    public partial class EnterScoreMenu : UserControl
    {

        public EnterScoreMenu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Speichert die erreichte Punktzahl unter dem eingegebenen Namen in der Highscore und wechselt zum Hauptmenue
        /// </summary>
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            GameManager.Instance.ExitLevel(true, NameValue.Text);
        }

        /// <summary>
        /// Wechselt zum Hauptmenue ohne einen Eintrag in der Highscore zu erzeugen
        /// </summary>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            GameManager.Instance.ExitLevel(false, null);
        }

        /// <summary>
        /// Aktualisiert die Ausgabe der erreichten Punktzahl
        /// </summary>
        public void UpdateScoreValue(int score)
        {
            // Score ausgeben
            ScoreValue.Content = score.ToString();
        }

        /// <summary>
        /// Befuellt die Rangliste mit den (der erreichten Punktzahl am naechsten kommenden) Highscoreeintraegen,
        /// so dass dem Benutzer die direkten Plaetze vor und hinter seiner erreichten Platzierung angezeigt werden.
        /// </summary>
        public void UpdateHighScoreSnippet(int score)
        {
            // Highscoreliste in Eintraege mit hoeherer Punktzahl und niedrigerer Punktzahl als der erreichten Punktzahl unterteilen
            ReadOnlyCollection<HighScoreEntry> fullHighScore = SaveManager.Instance.HighScore;
            List<HighScoreEntry> upperPart = new List<HighScoreEntry>();
            List<HighScoreEntry> lowerPart = new List<HighScoreEntry>();
            foreach (HighScoreEntry entry in fullHighScore)
            {
                if (entry.Score > score)
                    upperPart.Add(entry);
                else
                    lowerPart.Add(entry);
            }

            // Die Teillisten nach Punktzahl sortieren
            List<HighScoreEntry> sortedUpperPart = upperPart.OrderByDescending(o => o.Score).ToList();
            List<HighScoreEntry> sortedLowerPart = lowerPart.OrderByDescending(o => o.Score).ToList();

            // Festlegen, wie viele der "besseren" und wie viele der "schlechteren" Platzierungen angezeigt werden sollen.
            // So dass immer maximal 4 Eintraege angezeigt werden.
            int maxNumOfUppers = sortedLowerPart.Count < 2 ? 4 - sortedLowerPart.Count : 2;
            int maxNumOfLowers = sortedUpperPart.Count < 2 ? 4 - sortedUpperPart.Count : 2;

            // Die "besseren" Eintraege zur Ausgabe hinzufuegen
            HighScoreListView.Items.Clear();
            if (sortedUpperPart.Count <= maxNumOfUppers)
                foreach (HighScoreEntry item in sortedUpperPart)
                    HighScoreListView.Items.Add(item);
            else
                for (int i = sortedUpperPart.Count - maxNumOfUppers; i < sortedUpperPart.Count; i++)
                    HighScoreListView.Items.Add(sortedUpperPart[i]);

            // Den eigenen Eintrag zur Ausgabe hinzufuegen und diesen auswaehlen um ihn hervorzuheben
            int rank = sortedUpperPart.Count + 1;
            RankValue.Content = rank.ToString()+".";
            HighScoreEntry placeHolder = new HighScoreEntry("", score, rank);
            HighScoreListView.Items.Add(placeHolder);
            HighScoreListView.SelectedItems.Clear();
            HighScoreListView.SelectedItems.Add(placeHolder);

            // Die "schlechteren" Eintraege zu Ausgabe hinzufuegen
            if (sortedLowerPart.Count <= maxNumOfLowers)
                foreach (HighScoreEntry item in sortedLowerPart)
                    HighScoreListView.Items.Add(new HighScoreEntry(item.Name, item.Score, item.Rank+1));
            else
                for (int i = 0; i < maxNumOfLowers; i++)
                    HighScoreListView.Items.Add(new HighScoreEntry(sortedLowerPart[i].Name, sortedLowerPart[i].Score, sortedLowerPart[i].Rank + 1));
        }

        /// <summary>
        /// Passt die Spaltenbreiten der Highscoretabelle prozentual an
        /// </summary>
        private void CalcColumnWidths(object sender, SizeChangedEventArgs e)
        {

            ListView listView = sender as ListView;
            GridView gView = listView.View as GridView;

            // 100% width of listview
            var workingWidth = listView.ActualWidth - SystemParameters.VerticalScrollBarWidth; // take into account vertical scrollbar

            // width of columns in percentage (100% in sum, tested values) 
            var col1 = 0.1;
            var col2 = 0.495;
            var col3 = 0.495;

            gView.Columns[0].Width = workingWidth * col1;
            gView.Columns[1].Width = workingWidth * col2;
            gView.Columns[2].Width = workingWidth * col3;
        }

        /// <summary>
        /// Legt UI-Fokus auf das Namens-Texteingabefeld
        /// </summary>
        private void FocusInput(object sender, TouchEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            FocusManager.SetFocusedElement(tb.Parent, tb);
        }
    }
}
