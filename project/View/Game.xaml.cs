﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using ProtonCollider.Controller;
using ProtonCollider.Model;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Threading;

namespace ProtonCollider.View
{
    /// <summary>
    /// Interaktionslogik für Game.xaml
    /// </summary>
    public partial class Game : UserControl
    {
        // Time running out variables
        private const string _REDCLOCK_IMAGE_SRC = @"/Resources/icons/clock_red.png";
        private const string _WHITECLOCK_IMAGE_SRC = @"/Resources/icons/clock.png";
        private static BitmapImage redClockSource;
        private static BitmapImage whiteClockSource;
        private static SolidColorBrush redColorBrush;
        private static SolidColorBrush whiteColorBrush;
        private static Storyboard blinkStoryBoard;
        private static DoubleAnimation blinkAnimation;
        private static bool blinkAnimationInitialised = false;
        private static bool isBlinking = false;

        // static Constructor / static Initializer
        static Game()
        {
            // laden der benoetigten dynamischen GUI-Ressourcen
            redClockSource = new BitmapImage();
            redClockSource.BeginInit();
            redClockSource.UriSource = new Uri(_REDCLOCK_IMAGE_SRC, UriKind.Relative);
            redClockSource.EndInit();

            whiteClockSource = new BitmapImage();
            whiteClockSource.BeginInit();
            whiteClockSource.UriSource = new Uri(_WHITECLOCK_IMAGE_SRC, UriKind.Relative);
            whiteClockSource.EndInit();

            redColorBrush = new SolidColorBrush(Colors.Red);
            whiteColorBrush = new SolidColorBrush(Colors.White);
        }

        public Game()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Aktualisiert die Ausgabe der aktuell erreichten Punktzahl
        /// </summary>
        public void UpdateScoreValue(int score)
        {
            // Score ausgeben
            ScoreValue.Content = score.ToString("D7");
        }

        /// <summary>
        /// Aktualisiert die Anzeige der verbleibenden Rundenzeit.
        /// Wenn timeIsRunningOut = true, wird die Ausgabe in rot dargestellt 
        /// und eine Blinkanimation wird auf die Zeitanzeige angewandt
        /// </summary>
        public void UpdateTimerValue(TimeSpan timerValue, bool timeIsRunningOut)
        {
            // Timer ausgeben
            TimerValue.Content = timerValue.Minutes.ToString("#0") + ":" + timerValue.Seconds.ToString("#00") + ":" + timerValue.Milliseconds.ToString("#00");

            // Time Running out? -> change color to red
            ViewManager.Instance.GameView.ClockImage.Source = timeIsRunningOut ? redClockSource : whiteClockSource;
            ViewManager.Instance.GameView.TimerValue.Foreground = timeIsRunningOut ? redColorBrush : whiteColorBrush;

            if (!blinkAnimationInitialised)
                InitBlinkAnimation();
            
            // Pause blinking, if currently blinking and time is not running out
            if (isBlinking && !timeIsRunningOut)
            {
                blinkStoryBoard.Stop();
                isBlinking = false;
            }
            // Start blinking, if currently not blinkung and time is running out
            else if (!isBlinking && timeIsRunningOut)
            {
                blinkStoryBoard.Begin();
                SoundManager.Instance.LoopRunningOutSound(true);
                isBlinking = true;
            }
        }

        /// <summary>
        /// Bereitet BlinkAnimation der Zeitanzeige vor
        /// </summary>
        private void InitBlinkAnimation()
        {
            Duration duration = new Duration(TimeSpan.FromSeconds(0.2));
            blinkAnimation = new DoubleAnimation(1, 0, duration);
            blinkStoryBoard = new Storyboard();
            blinkStoryBoard.Children.Add(blinkAnimation);
            blinkStoryBoard.RepeatBehavior = RepeatBehavior.Forever;
            blinkStoryBoard.AutoReverse = true;

            Storyboard.SetTarget(blinkAnimation, ViewManager.Instance.GameView.TimerValue);
            Storyboard.SetTargetProperty(blinkAnimation, new PropertyPath(Label.OpacityProperty));
            blinkStoryBoard.Begin();
            blinkStoryBoard.Pause();

            blinkAnimationInitialised = true;
        }

        /// <summary>
        /// Laesst das Spiel pausieren und zeigt das Pausemenue an
        /// falls es gerade laeuft, und umgekehrt.
        /// </summary>
        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (!GameManager.Instance.PauseMenu.IsActive)
                GameManager.Instance.Pause();
            else
                GameManager.Instance.Resume();
        }

        /// <summary>
        /// Zeichnet einen Kollisionsbereich der gegebenen Hoehe und Breite
        /// an der gegebenen Position. Die Position bestimmt die
        /// obere linke Ecke des Kollisionsbereichs
        /// </summary>
        public void DrawAreaBorder(double height, double width, Point position)
        {
            Color lightBlue = (Color)ColorConverter.ConvertFromString("#55ccf2");

            // Erzeugen der Border, die den Kollisionsbereich markiert
            Border border = new Border();
            border.BorderThickness = new Thickness(3);
            border.BorderBrush = new SolidColorBrush(lightBlue);
            border.BorderBrush.Opacity = 0.25;
            border.CornerRadius = new CornerRadius(15);
            border.Height = height;
            border.Width = width;

            // Kollisionsbereich der Ausgabe / dem Gamescreen hinzufuegen
            Canvas.SetLeft(border, position.X);
            Canvas.SetTop(border, position.Y);
            ViewManager.Instance.GameView.GameCanvas.Children.Add(border);
        }

        /// <summary>
        /// Zeichnet den uebergebenen Text, horizontal zentriert auf gegebener
        /// vertikalen Position.
        /// </summary>
        public void DrawMultiplierLabel(double yPos, String text)
        {
            // Erzeugen des Labels
            Color lightBlue = (Color)ColorConverter.ConvertFromString("#55ccf2");
            Label label = new Label();
            label.Foreground = new SolidColorBrush(lightBlue);
            label.FontFamily = new FontFamily("Courier New");
            label.FontSize = 50;
            label.Content = text;
            label.Opacity = 0.25;

            // Rendern des Labels
            ViewManager.Instance.GameView.GameCanvas.Children.Add(label);

            // Positionionierung des Labels nachdem es gerendert wurde (um tatsaechliche Renderbreite parat zu haben)
            label.Loaded += (s, args) =>
            {
                double horizontalCenter = (MainWindow.DESIGNWIDTH / 2) - (label.ActualWidth / 2);
                Canvas.SetLeft(label, horizontalCenter);
                Canvas.SetTop(label, yPos);
            };
            
        }
    }
}
