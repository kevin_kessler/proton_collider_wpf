﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.IO;
using System.Windows.Input;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Timers;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using ProtonCollider.Controller;
using Microsoft.Surface.Presentation.Generic;

namespace ProtonCollider.Model
{
    public enum Player { One, Two };
    public enum ProtonState {
        Initialised,
        Accelerating,
        Moving,
        Destroyed
    };

    
    /// <summary>
    /// The proton which is visible on the screen. The class stores the behaviour of the protons.
    /// The behaviour is implemented by using different states. In course of the life cycle of a proton
    /// it will enter several states. On entering a state appropriate actions will be taken, see <see cref="OnStateChanged"/> method.
    /// </summary>
    public class Proton : ScatterViewItem
    {
        private const string PROTON_VIDEO_SRC = @"Resources/proton_animation.mp4";
        
        public const int PROTON_RADIUS = 150;

        private ProtonManager protonManager;
        private MediaElement protonVideo;
        private ProtonState state;
        private Player player;
        private ProtonTracker protonTracker;
        private Point initialCenterForResettingProton;

        // Members for rotating animation of the proton
        private const double MAX_ROTATION_SPEED_RATIO = 4.0;
        private const double ROTATION_SPEED_UP_FACTOR = 1.3;  /// Responsible for accelerating the rotation
        private double currentRotationSpeedRatio = 1;
        private Storyboard rotationStoryboard;
        Timer timerToSpeedUpRotation = new Timer();

        // Members for moving animation of the proton
        private Storyboard movingStoryboard;
        private Point directionVector;
        private double acceleration;
       

        public Player Player
        {
            get
            {
                return player;
            }
            private set
            {
                player = value;
            }
        }

        private ProtonState State
        {
            get
            {
                return state;
            }
            set
            {
                ProtonState previousState = state;
                state = value; 
                OnStateChanged(previousState);
            }
        }


        public Proton(Player player, ProtonManager manager)
        {
            this.protonManager = manager;
            this.state = ProtonState.Destroyed;  // Set explicitly the attribute and not the property
            this.protonTracker = new ProtonTracker();
            this.Player = player;
            this.CanRotate = true;
            this.CanScale = false;
            this.CanMove = false;

            this.Height = this.Width = PROTON_RADIUS * 2;

            // Clip viewable content to a circle
            double center = this.Height / 2;
            double radius = this.Height / 3.9;  // 3.9 is a tested value
            EllipseGeometry circle = new EllipseGeometry(new Point(center, center), radius, radius);
            this.Clip = circle;

            // Set the Content to the video.
            SetProtonVideo();
        }

        private void SetProtonVideo()
        {
            // Create a MediaElement object.
            protonVideo = new MediaElement();
            protonVideo.LoadedBehavior = MediaState.Manual;
            protonVideo.UnloadedBehavior = MediaState.Manual;

            protonVideo.Source = new Uri(PROTON_VIDEO_SRC, UriKind.Relative);
            protonVideo.Play();

            protonVideo.MediaEnded += (s, args) =>
            {
                protonVideo.Position = TimeSpan.FromSeconds(0);
                protonVideo.Play();
            };
            this.Content = protonVideo;
        }


        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            this.State = ProtonState.Accelerating;
            base.OnMouseDown(e);
        }
        protected override void OnTouchDown(TouchEventArgs e)
        {
            this.State = ProtonState.Accelerating;
            base.OnTouchDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed || (this.state != ProtonState.Accelerating))
            {
                e.Handled = true;
                return;
            }

            OnMove(e.GetPosition(ViewManager.Instance.GameView));
            base.OnMouseMove(e);
        }

        protected override void OnTouchMove(TouchEventArgs e)
        {
            if (this.state != ProtonState.Accelerating)
            {
                e.Handled = true;
                return;
            }

            OnMove(e.GetTouchPoint(ViewManager.Instance.GameView).Position);
            base.OnTouchMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            OnTouchUp(e.GetPosition(ViewManager.Instance.GameView));
            base.OnMouseUp(e);
        }
        protected override void OnTouchUp(TouchEventArgs e)
        {
            OnTouchUp(e.GetTouchPoint(ViewManager.Instance.GameView).Position);
            base.OnTouchUp(e);
        }

        private void OnTouchUp(Point touchUpPoint)
        {
            directionVector = protonTracker.GetDirectionVector();
            acceleration = protonTracker.GetAcceleration(directionVector);

            if (2000 > acceleration) // 2000 is a tested value
            {
                this.State = ProtonState.Initialised;
            }
            if (ProtonState.Accelerating == this.State)
            {
                this.State = ProtonState.Moving;
            }
        }

        private void OnMove(Point movedPosition)
        {
            Debug.Assert(ProtonState.Accelerating == this.State);

            this.Center = movedPosition;

            protonTracker.AddMovePoint(this.Center);

            if (this.ReachedAccelerationBorder())
                this.State = ProtonState.Moving;
        }

        /// <summary>
        /// Determines if the proton reached the Acceleration Border.
        /// The acceleration border determines the area in which the user can control the proton and depends
        /// on the player (left=one, right=two).
        /// If the user moves a proton to this border then the proton will go into the moving state.
        /// </summary>
        /// <returns>true if the proton reaches the border of the acceleration area</returns>
        private bool ReachedAccelerationBorder()
        {
            double gameViewHeight = MainWindow.DESIGNHEIGHT;
            double gameViewWidth = MainWindow.DESIGNWIDTH;

            double deltaX;
            double deltaY;

            if (this.player == Player.One)
                deltaX = this.ActualCenter.X;
            else
                deltaX = gameViewWidth - this.ActualCenter.X;
            deltaY = gameViewHeight - this.ActualCenter.Y;

            double squaredNorm = deltaX * deltaX + deltaY * deltaY;
            
            double GAME_VIEW_DIAGONAL = Math.Sqrt(gameViewHeight*gameViewHeight + gameViewWidth*gameViewWidth);
            double MAX_NORM = GAME_VIEW_DIAGONAL * 0.25;
            return squaredNorm > MAX_NORM * MAX_NORM;
        }

        /// <summary>
        /// Action after a state change. Can be used to initiate initiating and closing (of the old state) 
        /// operations of the respective state.
        /// </summary>
        private void OnStateChanged(ProtonState previousState)
        {

            // closing actions
            switch (this.State)
            {
                case ProtonState.Initialised:
                    break;
                case ProtonState.Accelerating:
                    break;
                case ProtonState.Moving:
                    break;
                case ProtonState.Destroyed:
                    break;
            }

            // initiating actions
            switch (this.State)
            {
                case ProtonState.Initialised:
                    StopLoadingIfNecessary();
                    ResetToStartState();
                    this.Visibility = Visibility.Visible;
                    break;
                case ProtonState.Accelerating:
                    InitiateAcceleration();
                    break;
                case ProtonState.Moving:
                    InitiateMoving();
                    protonManager.CreateNewProton(Player);
                    break;
                case ProtonState.Destroyed:
                    ResetToStartState();
                    this.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private void StopLoadingIfNecessary()
        {
            if (null != rotationStoryboard)
                rotationStoryboard.Pause(this);
        }

        private void InitiateAcceleration() 
        {
            Debug.Assert(ProtonState.Accelerating == this.State);

            // If we have no storyboard yet then we create one (per Proton) else we resume the paused storyboard
            if (null == rotationStoryboard)
            {
                // Animation
                Duration duration = new Duration(TimeSpan.FromSeconds(1));
                DoubleAnimation rotationAnimation = new DoubleAnimation(0.0, 360.0, duration);
                rotationAnimation.RepeatBehavior = RepeatBehavior.Forever;
                rotationAnimation.AutoReverse = false;

                // Storyboard
                rotationStoryboard = new Storyboard();
                rotationStoryboard.Children.Add(rotationAnimation);
                Storyboard.SetTarget(rotationAnimation, this);
                Storyboard.SetTargetProperty(rotationAnimation, new PropertyPath(ScatterViewItem.OrientationProperty));
                rotationStoryboard.Begin(this, true);

                // Timer to increase speed of rotation to indicate the loading process
                timerToSpeedUpRotation = new Timer();
                timerToSpeedUpRotation.Elapsed += (s, args) =>
                    {
                        if (currentRotationSpeedRatio < MAX_ROTATION_SPEED_RATIO)
                        {
                            currentRotationSpeedRatio *= ROTATION_SPEED_UP_FACTOR;
                            rotationStoryboard.SetSpeedRatio(this, currentRotationSpeedRatio);
                        }
                    };
                timerToSpeedUpRotation.Interval = 100;  // [ms]
                timerToSpeedUpRotation.Enabled = true;
            }
            else
            {
                // Reset rotation speed and enable timer again
                currentRotationSpeedRatio = 1;
                rotationStoryboard.SetSpeedRatio(this, currentRotationSpeedRatio);
                timerToSpeedUpRotation.Enabled = true;

                rotationStoryboard.Resume(this);
            } 
        }

        private void InitiateMoving()
        {
            Debug.Assert(ProtonState.Moving == this.State);

            StartMoveAnimation();
        }

        /// <summary>
        /// A debug class to examine the move animation.
        /// </summary>
        class DEBUG_AnimationTracker
        {
            public Point startPoint;
            public Point endPoint;
            public Point initialDirectionVector;
            public Point initialDirectionVectorWithEnergy;
            public Point resultingDirectionVector;
            public double accelerationEnergy;
            public double rotationEnergy;
            public double acceleration;
            public bool adjusted = false;

            internal void print()
            {
                String report = "###################################\n";
                report += "StartPoint: " + startPoint;
                report += "\nEndPoint: " + endPoint;
                report += "\ninitialDirectionVector: " + initialDirectionVector;
                report += "\ninitialDirectionVectorWithEnergy: " + initialDirectionVectorWithEnergy;
                report += "\nresultingDirectionVector: " + resultingDirectionVector;
                report += "\naccelerationEnergy: " + accelerationEnergy;
                report += "\nrotationEnergy: " + rotationEnergy;
                report += "\ninitial acceleration: " + acceleration;
                report += "\nadjusted: " + adjusted;
                report += "\n===========================================";
                Console.WriteLine(report);
            }
        }

        private void StartMoveAnimation()
        {
            // Start moving animation
            PointAnimation movingAnimation = new PointAnimation();
            movingAnimation.From = this.ActualCenter;

            DEBUG_AnimationTracker debugTracker = new DEBUG_AnimationTracker();
            debugTracker.startPoint = this.ActualCenter;


            directionVector = protonTracker.GetDirectionVector();
            acceleration = protonTracker.GetAcceleration(directionVector);

            debugTracker.initialDirectionVector = directionVector;
            debugTracker.acceleration = acceleration;

            const double MAX_ACCEPTED_ACCELERATION = 50000;
            double accelerationEnergy = 5 * (Math.Min(acceleration, MAX_ACCEPTED_ACCELERATION) / MAX_ACCEPTED_ACCELERATION);
            double rotationEnergy = currentRotationSpeedRatio;
            double deltaX = directionVector.X * accelerationEnergy * rotationEnergy;
            double deltaY = directionVector.Y * accelerationEnergy * rotationEnergy;

            const double MIN_DELTA = 1700; // 1700 Corresponds to min way to move in pixels (either x or y direction)

            debugTracker.initialDirectionVectorWithEnergy = new Point(deltaX, deltaY);
            // Check to assure the proton moves outside the screen
            // Idea: if the way is too short then set a min delta.
            double absDeltaX = Math.Abs(deltaX);
            double absDeltaY = Math.Abs(deltaY);
            if (Math.Max(absDeltaX, absDeltaY) < MIN_DELTA)
            {
                if (absDeltaX > absDeltaY)
                { // Set deltaX to a min delta
                    double tmpDeltaX = deltaX < 0 ? -MIN_DELTA : MIN_DELTA;  // To handle negative delta 
                    deltaY = tmpDeltaX * deltaY / deltaX;  // Keeps the direction of the vector 
                    deltaX = tmpDeltaX;
                }
                else
                { // Set deltaY to a min delta
                    double tmpDeltaY = deltaY < 0 ? -MIN_DELTA : MIN_DELTA;  // To handle negative delta
                    deltaX = tmpDeltaY * deltaX / deltaY;  // Keeps the direction of the vector
                    deltaY = tmpDeltaY;
                }
                debugTracker.adjusted = true;                
            }

            debugTracker.rotationEnergy = rotationEnergy;
            debugTracker.accelerationEnergy = accelerationEnergy;
            debugTracker.resultingDirectionVector = new Point(deltaX, deltaY);

            double toX = this.ActualCenter.X + deltaX;
            double toY = this.ActualCenter.Y + deltaY;

            debugTracker.endPoint = new Point(toX, toY);

            movingAnimation.To = new Point(toX, toY);
            Storyboard.SetTarget(movingAnimation, this);
            Storyboard.SetTargetProperty(movingAnimation, new PropertyPath(ScatterViewItem.CenterProperty));

            movingStoryboard = new Storyboard();
            movingStoryboard.Children.Add(movingAnimation);
            movingStoryboard.FillBehavior = FillBehavior.Stop;
            movingStoryboard.Completed += (s, args) =>
            {
                this.Center = this.ActualCenter;
                this.State = ProtonState.Destroyed;
            };
            movingStoryboard.Begin();

            debugTracker.print();
        }

        private void ResetToStartState() 
        {
            if(movingStoryboard != null)
                movingStoryboard.Stop();
            this.Center = this.initialCenterForResettingProton;
            currentRotationSpeedRatio = 1;
            timerToSpeedUpRotation.Enabled = false;
        }

        internal void SetInitialCenter(Point point)
        {
            this.Center = point;
            this.initialCenterForResettingProton = point;
        }

        internal void Destroy()
        {
            this.State = ProtonState.Destroyed;
        }

        internal double GetVelocityEnergy()
        {
            return acceleration;   
        }

        internal double GetRotationEnergy()
        {
            return currentRotationSpeedRatio;
        }

        internal void Initialize()
        {
            State = ProtonState.Initialised;
        }

        internal bool IsNotDestroyed()
        {
            return ! IsDestroyed();
        }

        private bool IsDestroyed()
        {
            return ProtonState.Destroyed == State;
        }

        internal bool IsNotMoving()
        {
            return ! IsMoving();
        }

        public bool IsMoving()
        {
            return this.State == ProtonState.Moving;
        }
    }
    
}
