﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProtonCollider.Model
{
    /// <summary>
    /// Datenklasse / Modelklasse der Einzelnen Levels
    /// Wurde entkoppelt um in Future Work darauf aufsetzen zu koennen 
    /// und das Spiel um verschiedene Levels erweitern zu koennen.
    /// </summary>
    class Level
    {
        private int levelDuration;
        private int currentScore;

        public Level(int duration)
        {
            this.levelDuration = duration;
            this.currentScore = 0;
        }

        public int LevelDuration
        {
            get { return this.levelDuration; }
            private set { }
        }

        public int CurrentScore
        {
            get { return this.currentScore; }
            set { this.currentScore = value; }
        }

    }
}
