﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using ProtonCollider.Controller;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

namespace ProtonCollider.Model
{
    /// <summary>
    /// Responsible for managing the protons on a very abstract layer.
    /// It will be supported by further managers ander helpers (CollisionManager, Positioner and Creator).
    /// This class uses the singleton pattern.
    /// </summary>
    public class ProtonManager
    {

        private static ProtonManager protonManager;

        private ProtonCollisionManager collisionManager;
        private ProtonPositioner protonPositioner;
        private ProtonCreator protonCreator;

        public static ProtonManager Instance
        {
            get
            {
                if (protonManager == null)
                    ProtonManager.Init();
                return protonManager;
            }
            private set
            {
                protonManager = value;
            }
        }

        public static void Init()
        {
             if (protonManager == null)
                    protonManager = new ProtonManager();
        }

        private ProtonManager()
        {
            protonPositioner = new ProtonPositioner();
            protonCreator = new ProtonCreator(this, protonPositioner);
            collisionManager = new ProtonCollisionManager(this);
        }

        internal void StartDetection()
        {
            collisionManager.StartDetection();
        }

        internal void StopDetection()
        {
            collisionManager.StopDetection();
        }

        internal void HideProtons()
        {
            protonCreator.DestroyProtons();
        }

        internal void ShowOneProtonPerPlayer()
        {
            protonCreator.InitializeAndShowOneProtonPerPlayer();
        }

        internal void ResetProtonPositions()
        {
            protonCreator.ResetProtonPositions();
                
        }

        internal void DestroyProtons()
        {
            protonCreator.DestroyProtons();
        }

        internal void CreateNewProton(Player player)
        {
            protonCreator.ShowNextProton(player);
        }

        internal IEnumerable<Proton> PlayerOneProtons()
        {
            return protonCreator.ProtonsAsListOf(Player.One);
        }

        internal IEnumerable<Proton> PlayerTwoProtons()
        {
            return protonCreator.ProtonsAsListOf(Player.Two);
        }

        internal void ResetProtons()
        {
            protonCreator.DestroyProtons();
            protonCreator.InitializeAndShowOneProtonPerPlayer();
        }
    }


    /// <summary>
    /// Responsible for setting the position of protons (on the screen), depending on the number of the user.
    /// </summary>
    public class ProtonPositioner
    {
        /// <summary>
        /// The parameter describes the postion of the Proton on the screen
        /// along the diagonal.
        /// If we have a diagonal of 100px and a value of 10 percent, then
        /// the Proton will be set to 10 px from the lower corner.
        /// </summary>
        private const int PERCENTAGE_OF_DIAGONAL = 10;

        /// <summary>
        /// This constant is a value to adjust the y-position of the proton.
        /// E.g.: If the calculated y-position is 100 and delta y-position is 10
        ///       then the adjusted y-position is 110.
        /// </summary>
        private const int DELTA_Y_POSITION = -45;

        public ProtonPositioner()
        {
        }

        public void SetPositionOf(Proton proton)
        {
            double diagonal = Math.Sqrt(MainWindow.DESIGNHEIGHT * MainWindow.DESIGNHEIGHT + MainWindow.DESIGNWIDTH * MainWindow.DESIGNWIDTH);
            double angleOfDiagonal = Math.Atan(MainWindow.DESIGNHEIGHT / MainWindow.DESIGNWIDTH); // atan(opposite/adjacent)
            double diagonalPositon = diagonal * ((Double)PERCENTAGE_OF_DIAGONAL / 100);
            double distanceFromX = diagonalPositon * Math.Cos(angleOfDiagonal);
            double distanceFromY = diagonalPositon * Math.Sin(angleOfDiagonal);

            double yPosition = MainWindow.DESIGNHEIGHT - distanceFromY + DELTA_Y_POSITION;

            switch (proton.Player)
            {
                case Player.One:
                    // lower left corner
                    proton.SetInitialCenter(new Point(distanceFromX, yPosition));
                    break;
                case Player.Two:
                    // lower right corner
                    proton.SetInitialCenter(new Point(MainWindow.DESIGNWIDTH - distanceFromX, yPosition));
                    break;
                default:
                    throw new NotImplementedException("Added a new user?");
            }
        }

    }


    /// <summary>
    /// This class is responsible for creating and pooling of Protons.
    /// </summary>
    class ProtonCreator
    {
        private const int NR_OF_PROTONS_PER_PLAYER = 5;

        private ProtonPositioner protonPositioner;
        private Proton[] playerOneProtons;
        private int playerOnePointer = 0;  // Pointer to the current displayed Proton of player one
        private Proton[] playerTwoProtons;
        private int playerTwoPointer = 0;  // Pointer to the current displayed Proton of player two

        public ProtonCreator(ProtonManager manager, ProtonPositioner protonPositioner)
        {
            this.protonPositioner = protonPositioner;
            playerOneProtons = new Proton[NR_OF_PROTONS_PER_PLAYER];
            playerTwoProtons = new Proton[NR_OF_PROTONS_PER_PLAYER];

            for(int i=0; i < NR_OF_PROTONS_PER_PLAYER ;++i)
            {
                playerOneProtons[i] = new Proton(Player.One, manager);
                playerTwoProtons[i] = new Proton(Player.Two, manager);
            }
            ResetProtonPositions();
            DestroyProtons();
        }

        public void ShowNextProton(Player player)
        {
            if (Player.One == player)
            {
                playerOnePointer = (playerOnePointer + 1) % NR_OF_PROTONS_PER_PLAYER;
                playerOneProtons[playerOnePointer].Initialize();
            }else
            {
                playerTwoPointer = (playerTwoPointer + 1) % NR_OF_PROTONS_PER_PLAYER;
                playerTwoProtons[playerTwoPointer].Initialize();
            }
        }

        public void DestroyProtons()
        {
            for (int i = 0; i < NR_OF_PROTONS_PER_PLAYER; ++i)
            {
                playerOneProtons[i].Destroy();
                playerTwoProtons[i].Destroy();
            }
        }

        internal void InitializeAndShowOneProtonPerPlayer()
        {
            playerOneProtons[playerOnePointer].Initialize();
            playerTwoProtons[playerTwoPointer].Initialize();
        }

        internal void ResetProtonPositions()
        {
            for (int i = 0; i < NR_OF_PROTONS_PER_PLAYER; ++i)
            {
                protonPositioner.SetPositionOf(playerOneProtons[i]);
                protonPositioner.SetPositionOf(playerTwoProtons[i]);
            }
        }

        internal IEnumerable<Proton> ProtonsAsListOf(Player player)
        {
            Proton[] protons;

            if (Player.One == player)
                protons = playerOneProtons;
            else
                protons = playerTwoProtons;

            return protons.ToList<Proton>();
        }
    }

    /// <summary>
    /// The ProtonTracker tracks the position of the Proton by saving a specific amount(NR_POINTS_TO_TRACK) of points.
    /// These Points are used to calculate the direction and acceleration for the moving animation.
    /// </summary>
    class ProtonTracker
    {
        public const int NR_POINTS_TO_TRACK = 100;

        private int counterForTrackedPoints = 0;  /// Is used in the case when trackedPoints smaller NR_POINTS_TO_TRACK. Then we can only use trackedPoints for the computation.
        private int indexToNextInsertPosition = 0;
        private Point[] trackedPoints;

        public ProtonTracker()
        {
            trackedPoints = new Point[NR_POINTS_TO_TRACK];
        }

        /// <summary>
        /// Resets the tracker
        /// </summary>
        public void Reset()
        {
            counterForTrackedPoints = 0;
            indexToNextInsertPosition = 0;
        }

        public void AddMovePoint(Point pointToAdd)
        {
            trackedPoints[indexToNextInsertPosition] = pointToAdd;
            indexToNextInsertPosition = (indexToNextInsertPosition + 1) % NR_POINTS_TO_TRACK;
            ++counterForTrackedPoints;
        }

        /// <summary>
        /// Calculates the acceleration of the directionPoint.
        /// The acceleration corresponds to the squared norm (length) of the directionVector.
        /// We take the squared acceleration to avoid squareroot computation.
        /// We don't compute the directionVector from tracked points due to performance issues.
        /// (The direction vector has been calculated already, so we don't need to calculate it again) 
        /// </summary>
        /// <returns>Returns an acceleration factor > 1.0, (the squared norm of the given direction vector)</returns>
        public double GetAcceleration(Point directionVector)
        {
            double squaredNorm = directionVector.X * directionVector.X + directionVector.Y * directionVector.Y;
            return Math.Max(1.0, squaredNorm);
        }

        /// <summary>
        /// Calculates the direction of the last tracked points.
        /// (max. NR_POINTS_TO_TRACK).
        /// </summary>
        /// <returns>a Point </returns>
        public Point GetDirectionVector()
        {
            // We are calculating the direction vector by adding the corresponding delta (offset to the previous point)
            // to the new point in each direction (X,Y) separately.
            int minPoints = Math.Min(NR_POINTS_TO_TRACK, counterForTrackedPoints);
            double deltaX = 0.0;
            double deltaY = 0.0;
            int indexCurrentPoint;
            int indexPreviousPoint;
            if (NR_POINTS_TO_TRACK > counterForTrackedPoints)
            {  // We have tracked less than NR_POINTS_TO_TRACK 
                indexPreviousPoint = 0;

                for (int i = 1; i < counterForTrackedPoints; ++i)
                {
                    indexCurrentPoint = i;

                    deltaX += trackedPoints[indexCurrentPoint].X - trackedPoints[indexPreviousPoint].X;
                    deltaY += trackedPoints[indexCurrentPoint].Y - trackedPoints[indexPreviousPoint].Y;

                    indexPreviousPoint = indexCurrentPoint;
                }
            }
            else
            { // We have a full array of points.
                indexPreviousPoint = indexToNextInsertPosition;  // indexToNextInsertPosition is the oldest tracked point.

                for (int i = 1; i < NR_POINTS_TO_TRACK; ++i)
                {
                    indexCurrentPoint = (indexPreviousPoint + 1) % NR_POINTS_TO_TRACK;

                    deltaX += trackedPoints[indexCurrentPoint].X - trackedPoints[indexPreviousPoint].X;
                    deltaY += trackedPoints[indexCurrentPoint].Y - trackedPoints[indexPreviousPoint].Y;

                    indexPreviousPoint = indexCurrentPoint;
                }

            }

            return new Point(deltaX, deltaY);
        }
    }

}
