﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace ProtonCollider.Controller
{
    /// <summary>
    /// Diese Klasse Dient der Verwaltung der Musik und Soundeffekte.
    /// Bietet Methoden zum Abspielen der einzelnen Sounds / Musik.
    /// </summary>
    class SoundManager
    {
        // Relative Pfade zu den Soundfiles
        private const string _ROUND_MUSIC_SRC = @"Resources/Sound/Arcade_Just_in_Space_Sparkling.mp3";
        private const string _MENU_MUSIC_SRC = @"Resources/Sound/Intro_Just_in_Space_Sparkling.mp3";
        private const string _EXPLOSION_SOUND_SRC = @"Resources/Sound/implosion.wav";
        private const string _TIME_RUNNING_OUT_SRC = @"Resources/Sound/ticktock.wav";
        private const string _BUZZER_SRC = @"Resources/Sound/levelcomplete.wav";

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static SoundManager instance;
        public static SoundManager Instance
        {
            get
            {
                if (null == instance)
                    instance = new SoundManager();
                return instance;
            }

            private set { }
        }

        private MediaPlayer collisionSound;
        private MediaPlayer runningOutSound;
        private MediaPlayer buzzerSound;
        private MediaPlayer menuMusic;
        private MediaPlayer roundMusic;

        public MediaPlayer CollisionSound
        {
            get { return collisionSound; }
            private set { }
        }

        public MediaPlayer RunningOutSound
        {
            get { return runningOutSound; }
            private set { }
        }

        public MediaPlayer BuzzerSound
        {
            get { return buzzerSound; }
            private set { }
        }

        public MediaPlayer MenuMusic
        {
            get { return menuMusic; }
            private set { }
        }

        public MediaPlayer RoundMusic
        {
            get { return roundMusic; }
            private set { }
        }

        private SoundManager()
        {
            PrepareSound();
        }

        /// <summary>
        /// Laedt die Soundfiles in die zustaendigen MediaplayerObjekte
        /// </summary>
        private void PrepareSound()
        {
            collisionSound = new MediaPlayer();
            collisionSound.Open(new Uri(_EXPLOSION_SOUND_SRC, UriKind.Relative));
            collisionSound.Volume = 1;

            runningOutSound = new MediaPlayer();
            runningOutSound.Open(new Uri(_TIME_RUNNING_OUT_SRC, UriKind.Relative));
            runningOutSound.Volume = 1;

            buzzerSound = new MediaPlayer();
            buzzerSound.Open(new Uri(_BUZZER_SRC, UriKind.Relative));
            buzzerSound.Volume = 1;

            roundMusic = new MediaPlayer();
            roundMusic.Open(new Uri(_ROUND_MUSIC_SRC, UriKind.Relative));
            roundMusic.Volume = 0.5;

            menuMusic = new MediaPlayer();
            menuMusic.Open(new Uri(_MENU_MUSIC_SRC, UriKind.Relative));
            menuMusic.Volume = 0.5;

        }

        /// <summary>
        /// Spielt den Kollisionssound ab
        /// </summary>
        public void PlayCollisionSound()
        {
            collisionSound.Position = TimeSpan.Zero;
            collisionSound.Play();

            collisionSound.MediaEnded += (s, args) =>
            {
                collisionSound.Stop();
                collisionSound.Position = TimeSpan.Zero;
            };
        }

        /// <summary>
        /// Spielt den Level-Complete Sound ab
        /// </summary>
        public void PlayBuzzerSound()
        {
            buzzerSound.Position = TimeSpan.Zero;
            buzzerSound.Play();

            buzzerSound.MediaEnded += (s, args) =>
            {
                buzzerSound.Stop();
                buzzerSound.Position = TimeSpan.Zero;
            };
        }

        /// <summary>
        /// Startet die "Tick-Tack" Soundloop, welche signalisiert dass die Zeit des aktuellen Levels bald auslaeuft.
        /// Wenn "fromBeginning" = true, dann wird die loop von vorne gestartet, andernfalls fortgefuehrt.
        /// </summary>
        public void LoopRunningOutSound(bool fromBeginning)
        {
            if(fromBeginning)
                ResetRunningOutSound();

            runningOutSound.Play();

            runningOutSound.MediaEnded += (s, args) =>
            {
                runningOutSound.Stop();
                LoopRunningOutSound(true);
            };
        }

        public void ResetRunningOutSound()
        {
            runningOutSound.Position = TimeSpan.Zero;
        }

        /// <summary>
        /// Startet die "Levelmusik" Soundloop.
        /// Wenn "fromBeginning" = true, dann wird die loop von vorne gestartet, andernfalls fortgefuehrt.
        /// </summary>
        public void LoopRoundMusic(bool fromBeginning)
        {
            if (fromBeginning)
                ResetRoundMusic();

            roundMusic.Play();
            
            // repeat
            roundMusic.MediaEnded += (s, args) =>
            {
                roundMusic.Stop();
                LoopRoundMusic(true);
            };
        }

        public void ResetRoundMusic()
        {
            roundMusic.Position = TimeSpan.Zero;
        }

        /// <summary>
        /// Startet die "Menuemusik" Soundloop
        /// Wenn "fromBeginning" = true, dann wird die loop von vorne gestartet, andernfalls fortgefuehrt.
        /// </summary>
        public void LoopMenuMusic(bool fromBeginning)
        {
            if (fromBeginning)
                ResetMenuMusic();

            menuMusic.Play();

            // repeat
            menuMusic.MediaEnded += (s, args) =>
            {
                menuMusic.Stop();
                LoopMenuMusic(true);
            };
        }

        public void ResetMenuMusic()
        {
            menuMusic.Position = TimeSpan.Zero;
        }

    }
}
