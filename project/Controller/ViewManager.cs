﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtonCollider.Model;
using ProtonCollider.View;

namespace ProtonCollider.Controller
{
    /// <summary>
    /// Diese Klasse dient zur Verwaltung der einzelnen Screens.
    /// Sie bietet Methoden zum Einfachen laden eines anderen Screens.
    /// Ueber die Singleton Instanz koennen ausserdem die Instanzen der 
    /// einzelnen Screens abgerufen werden, um auf diese jederzeit Zugreifen zu koennen
    /// </summary>
    class ViewManager
    {
        /// <summary>
        /// Singleton Instanz
        /// </summary>
        private static ViewManager instance;
        public static ViewManager Instance
        {
            get
            {
                if (null == instance)
                    instance = new ViewManager();
                return instance;
            }

            private set { }
        }

        /// <summary>
        /// Enum zum typsicheren Wechseln zwischen den Screens
        /// </summary>
        public enum Views : int
        {
            MainMenu = 0,
            GameScreen = 1,
            HighScore = 2,
            EnterScoreMenu = 3,
            InfoScreen = 4
        }

        //Instanzen der einzelnen Screens
        private MainMenu mainMenu;
        private Game gameView;
        private HighScore scoreView;
        private EnterScoreMenu enterScoreMenu;
        private Info infoView;

        /// <summary>
        /// Konstruktor
        /// </summary>
        private ViewManager()
        {
            mainMenu = new MainMenu();
            gameView = new Game();
            scoreView = new HighScore();
            enterScoreMenu = new EnterScoreMenu();
            infoView = new Info();
        }

        public MainMenu MainMenu
        {
            get { return this.mainMenu; }
            private set { }
        }

        public Game GameView
        {
            get { return this.gameView; }
            private set { }
        }

        public HighScore ScoreView
        {
            get { return this.scoreView; }
            private set { }
        }

        public EnterScoreMenu EnterScoreMenu
        {
            get { return this.enterScoreMenu; }
            private set { }
        }

        public Info InfoView
        {
            get { return this.infoView; }
            private set { }
        }

        /// <summary>
        /// Wechselt zu dem Screen, der dem uebergebenen Enum-Wert entspricht.
        /// Startet ausserdem die entsprechende Hintergrundmusik des neuen Screens.
        /// </summary>
        /// <param name="view"></param>
        public void ChangeView(Views view)
        {
            bool comingFromGameView = MainWindow.Instance.ContentHolder.Content == gameView;
            if(comingFromGameView)
                SoundManager.Instance.RoundMusic.Stop();

            switch (view)
            {
                case Views.MainMenu:
                    SoundManager.Instance.LoopMenuMusic(comingFromGameView);
                    MainWindow.Instance.ContentHolder.Content = mainMenu;
                    break;

                case Views.GameScreen:
                    SoundManager.Instance.MenuMusic.Stop();
                    SoundManager.Instance.LoopRoundMusic(true);
                    MainWindow.Instance.ContentHolder.Content = gameView;
                    break;

                case Views.HighScore:
                    SoundManager.Instance.LoopMenuMusic(comingFromGameView);
                    MainWindow.Instance.ContentHolder.Content = scoreView;
                    break;

                case Views.EnterScoreMenu:
                    SoundManager.Instance.LoopMenuMusic(comingFromGameView);
                    MainWindow.Instance.ContentHolder.Content = enterScoreMenu;
                    break;

                case Views.InfoScreen:
                    SoundManager.Instance.LoopMenuMusic(comingFromGameView);
                    MainWindow.Instance.ContentHolder.Content = infoView;
                    break;
            }
        }
    }
}
